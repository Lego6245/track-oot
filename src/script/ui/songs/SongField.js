// frameworks
import Template from "/emcJS/util/html/Template.js";
import GlobalStyle from "/emcJS/util/html/GlobalStyle.js";
import Dialog from "/emcJS/ui/overlay/window/Dialog.js";


// GameTrackerJS
import StateDataEventManager from "/GameTrackerJS/ui/mixin/StateDataEventManager.js";
import Language from "/GameTrackerJS/util/Language.js";
// Track-OOT
import SongStateManager from "/script/state/song/StateManager.js"
import "./SongStave.js";
import "./SongBuilder.js";

const TPL = new Template(`
<div class="caption">
    <span id="title"></span>
    <button id="edit" class="button hidden" title="edit">✎</button>
    <button id="reset" class="button hidden" title="reset">↶</button>
    <button id="clear" class="button hidden" title="clear">✕</button>
</div>
<ootrt-stave id="stave"></ootrt-stave>
`);

const STYLE = new GlobalStyle(`
* {
    position: relative;
    box-sizing: border-box;
}
:host {
    display: inline-block;
    width: 500px;
    padding: 10px;
    margin: 5px;
    border: solid 2px white;
}
.caption {
    display: flex;
    align-items: center;
    height: 30px;
}
.button {
    height: 24px;
    color: white;
    background-color: black;
    border: solid 1px white;
    margin-left: 15px;
    font-size: 1em;
    appearance: none;
    cursor: pointer;
}
.button:hover {
    color: black;
    background-color: white;
}
.button.hidden {
    display: none;
}
`);

function editSong(event) {
    const builder = document.createElement("ootrt-songbuilder");
    builder.value = this.shadowRoot.getElementById("stave").value;
    const d = new Dialog({title: Language.generateLabel(this.ref), submit: true, cancel: true});
    d.addEventListener("submit", function(result) {
        if (result) {
            const state = this.getState();
            if (state != null) {
                state.notes = builder.value;
            }
        }
    }.bind(this));
    d.append(builder);
    d.show();
}

function resetSong(event) {
    const state = this.getState();
    if (state != null) {
        state.notes = null;
    }
}

function clearSong(event) {
    const state = this.getState();
    if (state != null) {
        state.notes = "";
    }
}

export default class HTMLTrackerSongField extends StateDataEventManager(HTMLElement) {
    
    constructor() {
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.append(TPL.generate());
        STYLE.apply(this.shadowRoot);
        /* --- */
        this.registerStateHandler("notes", event => {
            const staveEl = this.shadowRoot.getElementById("stave");
            if (staveEl != null) {
                staveEl.value = event.data;
            }
        });

        /* mouse events */
        const editEl = this.shadowRoot.getElementById("edit");
        editEl.onclick = editSong.bind(this);
        const resetEl = this.shadowRoot.getElementById("reset");
        resetEl.onclick = resetSong.bind(this);
        const clearEl = this.shadowRoot.getElementById("clear");
        clearEl.onclick = clearSong.bind(this);
    }

    applyDefaultValues() {
        const editEl = this.shadowRoot.getElementById("edit");
        if (editEl != null) {
            editEl.classList.add("hidden");
        }
        const resetEl = this.shadowRoot.getElementById("reset");
        if (resetEl != null) {
            resetEl.classList.add("hidden");
        }
        const clearEl = this.shadowRoot.getElementById("clear");
        if (clearEl != null) {
            clearEl.classList.add("hidden");
        }
        const staveEl = this.shadowRoot.getElementById("stave");
        if (staveEl != null) {
            staveEl.value = "";
        }
    }

    applyStateValues(state) {
        if (state != null) {
            const editEl = this.shadowRoot.getElementById("edit");
            if (editEl != null) {
                editEl.classList.toggle("hidden", !state.props.editable);
            }
            const resetEl = this.shadowRoot.getElementById("reset");
            if (resetEl != null) {
                resetEl.classList.toggle("hidden", !state.props.editable);
            }
            const clearEl = this.shadowRoot.getElementById("clear");
            if (clearEl != null) {
                clearEl.classList.toggle("hidden", !state.props.editable);
            }
            const staveEl = this.shadowRoot.getElementById("stave");
            if (staveEl != null) {
                staveEl.value = state.notes;
            }
        }
    }

    get ref() {
        return this.getAttribute("ref");
    }

    set ref(val) {
        this.setAttribute("ref", val);
    }

    static get observedAttributes() {
        return ["ref"];
    }
    
    attributeChangedCallback(name, oldValue, newValue) {
        if (oldValue != newValue) {
            const state = SongStateManager.get(this.ref);
            const titleEl = this.shadowRoot.getElementById("title");
            if (titleEl != null) {
                Language.applyLabel(titleEl, newValue);
            }
            this.switchState(state);
        }
    }

}

customElements.define("ootrt-songfield", HTMLTrackerSongField);
